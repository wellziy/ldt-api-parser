#how to build ldt api parser?
1. install make, gcc,g++,flex and bison.
2. using make to build the binary.
3. run the generated binary with 4 args,they are:
	* pkg\_file\_name: the pkg file name
	* output\_dir\_name: the output directory for the generated lua files
	* global\_module_name: the module name use to store global varibales and functions.usually it is named global.
	* auto\_file\_name: the output file use to store all to_TYPE functions for tolua++ bind and LDT autocomplete

#what does ldt api parser generated after it run?
* in ${out\_dir\_name} directory, it will generate a lua file for every class declared in pkg file, and a ${global\_module\_name}.lua file to store all global variables and functions declared in pkg file, and a ${auto\_file\_name}.lua to store all to_TYPE functions for tolua++ bind and LDT autocomplete.
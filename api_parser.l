%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "api_parser.tab.h"

void printMatch(const char* s)
{
	printf("match->%s\n",s);
}

%}

delim			[ \t\n]
ws				{delim}+

line_comment 	[/][/].*
block_comment  	[/][*]([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*[*]+[/]
comment			{line_comment}|{block_comment}
letter			[a-zA-Z]
digit			[0-9]
dec_number		[+-]?{digit}+(\.{digit}+)?(E[+-]?{digit}+)?[fF]?
hex_number		[0][x]({digit}|[a-fA-F])+	
bit_number		({dec_number}|{hex_number}){ws}*<<{ws}*({dec_number}|{hex_number})
number 			{dec_number}|{hex_number}|{bit_number}
using_namespace_declare		[$][Uu].*
one_line_function_body	[{].*[}]

not_quote		[^\"]
not_quote_pair	["]

cstring			["]{not_quote}*["]
identifier 		[_a-zA-Z][_a-zA-Z0-9]*

%%
"class"		{ printMatch(yytext); return CLASS; }
"struct"	{ printMatch(yytext); return STRUCT; }
 /*ignore them for convience*/
"const"		{ printMatch(yytext); }
"virtual"	{ printMatch(yytext); }
"extern"	{ printMatch(yytext); }
"unsigned"	{ printMatch(yytext); }
"signed"	{ printMatch(yytext); }
"static"	{ printMatch(yytext); return STATIC; }

 /*recognize them as IDENTIFIER for convience*/
 /*
"void"		{ printMatch(yytext); return VOID; }
"bool"		{ printMatch(yytext); return BOOL; }
"char"		{ printMatch(yytext); return CHAR; }
"short"		{ printMatch(yytext); return SHORT; }
"int"		{ printMatch(yytext); return INT; }
"long"		{ printMatch(yytext); return LONG; }
"long long"	{ printMatch(yytext); return LONGLONG; }

"float" 	{ printMatch(yytext); return FLOAT; }
"double" 	{ printMatch(yytext); return DOUBLE; }
"LUA_FUNCTION"	{ printMatch(yytext); return LUA_FUNCTION; }
 */

"#define" 	{ printMatch(yytext); return DEFINE; }
"typedef"	{ printMatch(yytext); return TYPEDEF; }
"enum"		{ printMatch(yytext); return ENUM; }
"public"	{ printMatch(yytext); return PUBLIC; }
"protected"	{ printMatch(yytext); return PROTECTED; }
"private" 	{ printMatch(yytext); return PRIVATE; }

"$#include"	{ printMatch(yytext); return PINCLUDE; }
"$pfile"	{ printMatch(yytext); return PFILE; }

 /*define some value*/
"false"		{ printMatch(yytext); return BOOL_VALUE; }
"true"		{ printMatch(yytext); return BOOL_VALUE; }
"NULL"		{ printMatch(yytext); return NULL_PTR; }
{cstring}	{ printMatch(yytext); yylval.c_string=strdup(yytext); return CSTRING; }
{number}	{ printMatch(yytext); return NUMBER; }

{comment}	{ printMatch(yytext); }
{using_namespace_declare}	{ printMatch(yytext); }
{identifier}	{ printMatch(yytext); yylval.c_string=strdup(yytext); return IDENTIFIER; }
{ws}		{ printMatch(yytext); }
[&]			{ printMatch(yytext); /*remove & to remove reference type*/ }
[~|]		{ printMatch(yytext); /*only alow these two logic operations for numbers*/ return *yytext; }
"@"			{ printMatch(yytext); /*alow tolua @ rename function*/ return *yytext; }
{one_line_function_body}	{ printMatch(yytext); /*allow one line function body implement in function declare*/ return ';'; }
[(){}\[\];:,=*]		{ printMatch(yytext); return *yytext; }
.			{ printMatch(yytext); printf("invalid input: %s!\n",yytext); return ERR; }
%%

//define all functions about yacc
int yywrap()
{
    return 1;
}

void yyerror(const char* s)
{
	printf("error: %s\n",s);
}

int parse_string(const char* str)
{
	YY_BUFFER_STATE buffer=yy_scan_string(str);
	int res=yyparse();
	yy_delete_buffer(buffer);

	return res;
}
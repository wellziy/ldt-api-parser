#pragma once

#include <string>
#include <vector>

class Field;
class Function;
class Module;
typedef std::vector<Field*> FieldList;
typedef std::vector<Function*> FunctionList;
typedef std::vector<Module*> ModuleList;
typedef std::vector<const char*> NameList;

class Module
{
public:
	const char* moduleName;
	FieldList* fieldList;
	FunctionList* functionList;
	NameList* nameList;

	bool isContainVariable(const char* variableName);
	bool isContainFunction(const char* functionName);

	FieldList* getMergedFieldList(ModuleList* moduleList);
	FunctionList* getMergedFunctionList(ModuleList* moduleList);
};
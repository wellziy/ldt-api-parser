#include "Module.hpp"
#include "Field.hpp"
#include "Param.hpp"
#include "Function.hpp"
#include <string.h>
#include "cpp_api.h"

bool Module::isContainVariable(const char* variableName)
{
	for(int i=0;i<fieldList->size();i++)
	{
		Field* field=(*fieldList)[i];
		if(strcmp(field->variableName,variableName)==0)
		{
			return true;
		}
	}
	return false;
}

bool Module::isContainFunction(const char* functionName)
{
	for(int i=0;i<functionList->size();i++)
	{
		Function* function=(*functionList)[i];
		if(strcmp(function->functionName,functionName)==0)
		{
			return true;
		}
	}
	return false;
}

FieldList* Module::getMergedFieldList(ModuleList* moduleList)
{
	FieldList* res=new FieldList();

	//add self fields
	for(int i=0;i<fieldList->size();i++)
	{
		Field* field=(*fieldList)[i];
		FieldListAddField(res,field);
	}

	//add all fields from parents
	for(int i=0;i<nameList->size();i++)
	{
		const char* parentName=(*nameList)[i];
		Module* parentModule=(Module*)ModuleListGetModuleByName(moduleList,parentName);
		if(parentModule==NULL)
		{
			continue;
		}
		FieldList* parentMergedFieldList=parentModule->getMergedFieldList(moduleList);
		for(int j=0;j<parentMergedFieldList->size();j++)
		{
			Field* pmf=(*parentMergedFieldList)[j];
			if(isContainVariable(pmf->variableName)==false)
			{
				FieldListAddField(res,pmf);
			}
		}
	}
	return res;
}

FunctionList* Module::getMergedFunctionList(ModuleList* moduleList)
{
	FunctionList* res=new FunctionList();
	//add self functions
	for(int i=0;i<functionList->size();i++)
	{
		Function* function=(*functionList)[i];
		FunctionListAddFunction(res,function);
	}
	//add all functions from parents
	for(int i=0;i<nameList->size();i++)
	{
		const char* parentName=(*nameList)[i];
		Module* parentModule=(Module*)ModuleListGetModuleByName(moduleList,parentName);
		if(parentModule==NULL)
		{
			continue;
		}
		FunctionList* parentMergedFunctionList=parentModule->getMergedFunctionList(moduleList);
		for(int j=0;j<parentMergedFunctionList->size();j++)
		{
			Function* pmf=(*parentMergedFunctionList)[j];
			//NOTICE: may be cause ambicular here
			if(strcmp(pmf->functionName,parentName)!=0&&isContainFunction(pmf->functionName)==false)
			{
				FunctionListAddFunction(res,pmf);
			}
		}
	}
	return res;
}
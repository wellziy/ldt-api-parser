#pragma once

#include <vector>
class Param;

class Function
{
public:
	const char* returnTypeName;
	const char* functionName;
	std::vector<Param*>* paramList;
};
#include "cpp_api.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <vector>
#include "Param.hpp"
#include "Function.hpp"
#include "Field.hpp"
#include "Module.hpp"

typedef std::vector<Param*> ParamList;
typedef std::vector<Function*> FunctionList;
typedef std::vector<Field*> FieldList;
typedef std::vector<const char*> NameList;
typedef std::vector<Module*> ModuleList;

//===========private functions=======================
static std::string getFileContent(const char* path)
{
	FILE* file=fopen(path,"r");
	if(file==NULL)
	{
		return "";
	}
	std::string res="";
	char buffer[1000];
	while(fgets(buffer,sizeof(buffer),file)!=NULL)
	{
		res+=std::string(buffer);
	}
	fclose(file);

	return res;
}

//dump functionns
static void dumpField(Field* field,Module* module)
{
	printf("\t@field %s %s\n",field->typeName,field->variableName);
}

static void dumpParam(Param* param,Function* function,Module* module)
{
	printf("\t\t@param %s %s\n",param->typeName,param->paramName);
}

static void dumpFunction(Function* function,Module* module)
{
	printf("\t@function %s\n",function->functionName);
	ParamList* paramList=function->paramList;
	for(int i=0;i<paramList->size();i++)
	{
		Param* param=(*paramList)[i];
		dumpParam(param,function,module);
	}
	printf("\t\t@return %s\n",function->returnTypeName);
}

static void dumpModule(Module* module)
{
	printf("@module %s\n",module->moduleName);
	FunctionList* functionList=module->functionList;
	for(int i=0;i<functionList->size();i++)
	{
		Function* function=(*functionList)[i];
		dumpFunction(function,module);
	}
	FieldList* fieldList=module->fieldList;
	for(int i=0;i<fieldList->size();i++)
	{
		Field* field=(*fieldList)[i];
		dumpField(field,module);
	}
}

//==========cpp api=============================
extern "C"
{
void yyerror(const char* s);
int parse_string(const char* str);
}

void showUseage()
{
	printf("usage:\n");
	printf("program input_file_name output_dir_name global_module_name auto_file_name\n");
}

char* typeNameForPointerOfType(const char* type)
{
	int len=strlen(type);
	char* typeName=new char[len+2];
	strcpy(typeName,type);
	typeName[len]='*';
	typeName[len+1]='\0';
	return typeName;
}

char* trimDoubleQuote(const char* name)
{
	std::string nameFull(name);
	std::string nameTrimQuote(nameFull.substr(1,nameFull.size()-2));
	return strdup(nameTrimQuote.c_str());
}

void parsePKG(const char* path)
{
	std::string content=getFileContent(path);
	if(content.size()<=0)
	{
		printf("Warning: the content of file %s is empty, may be oopen file failed\n",path);
		return;
	}	

	int res=parse_string(content.c_str());
	if(res)
	{
		printf("parse file failed %s\n",path);
	}
	else
	{
		printf("parse file succeed %s\n",path);
	}
}

/* store variables in field */
void* newField(const char* typeName,const char* variableName)
{
	Field* field=new Field();
	field->typeName=typeName;
	field->variableName=variableName;

	return field;
}

void* newFieldList()
{
	return new FieldList();
}

void FieldListAddField(void* fieldList,void* field)
{
	FieldList* fieldListPtr=(FieldList*)fieldList;
	Field* fieldPtr=(Field*)field;

	fieldListPtr->push_back(fieldPtr);
}

void FieldListMerge(void* fieldList,void* fieldList2)
{
	FieldList* fieldListPtr2=(FieldList*)fieldList2;
	for(int i=0;i<fieldListPtr2->size();i++)
	{
		FieldListAddField(fieldList,(*fieldListPtr2)[i]);
	}
}

/* store paramerters  in param*/
void* newParam(const char* typeName,const char* paramName,int hasDefaultValue)
{
	Param* param=new Param();
	param->typeName=typeName;
	param->paramName=paramName;
	param->hasDefaultValue=hasDefaultValue;

	return param;
}

void* newParamList()
{
	return new ParamList();
}

void ParamListAddParam(void* paramList,void* param)
{
	ParamList* paramListPtr=(ParamList*)paramList;
	Param* paramPtr=(Param*)param;
	paramListPtr->push_back(paramPtr);
}

void ParamListAddParamAtFront(void* paramList,void* param)
{
	ParamList* paramListPtr=(ParamList*)paramList;
	Param* paramPtr=(Param*)param;
	paramListPtr->insert(paramListPtr->begin(),paramPtr);
}

/* store functions in function */
void* newFunction(const char* returnTypeName,const char* functionName,void* paramList)
{
	Function* function=new Function();
	function->returnTypeName=returnTypeName;
	function->functionName=functionName;
	function->paramList=(ParamList*)paramList;

	return function;
}

void* FunctionGetParamList(void* function)
{
	Function* functionPtr=(Function*)function;
	return functionPtr->paramList;
}

void* newFunctionList()
{
	return new FunctionList();
}

void FunctionListAddFunction(void* functionList,void* function)
{
	FunctionList* functionListPtr=(FunctionList*)functionList;
	Function* functionPtr=(Function*)function;
	functionListPtr->push_back(functionPtr);
}

void* newNameList()
{
	return new NameList();
}

void NameListAddName(void* nameList,const char* name)
{
	NameList* nameListPtr=(NameList*)nameList;
	nameListPtr->push_back(name);
}

const char* NameListPopName(void* nameList)
{
	NameList* nameListPtr=(NameList*)nameList;
	if(nameListPtr->size()<=0)
	{
		return NULL;
	}
	const char* name=(*nameListPtr)[0];
	nameListPtr->erase(nameListPtr->begin());
	return name;
}

int NameListGetSize(void* nameList)
{
	NameList* nameListPtr=(NameList*)nameList;
	return nameListPtr->size();
}

/* store classes in module */
void* newModule(const char* moduleName)
{
	Module* module=new Module();
	module->moduleName=moduleName;
	module->fieldList=new FieldList();
	module->functionList=new FunctionList();
	module->nameList=new NameList();

	return module;
}

void ModuleSetModuleName(void* module,const char* moduleName)
{
	Module* modulePtr=(Module*)module;
	modulePtr->moduleName=moduleName;
}

void ModuleSetNameList(void* module,void* nameList)
{
	Module* modulePtr=(Module*)module;
	modulePtr->nameList=(NameList*)nameList;
}

void* newModuleList()
{
	return new ModuleList();
}

void ModuleListAddModule(void* moduleList,void* module)
{
	ModuleList* moduleListPtr=(ModuleList*)moduleList;
	Module* modulePtr=(Module*)module;
	moduleListPtr->push_back(modulePtr);
}

void* ModuleListGetModuleByName(void* moduleList,const char* moduleName)
{
	ModuleList* moduleListPtr=(ModuleList*)moduleList;
	for(int i=0;i<moduleListPtr->size();i++)
	{
		if(strcmp((*moduleListPtr)[i]->moduleName,moduleName)==0)
		{
			return (*moduleListPtr)[i];
		}
	}
	return NULL;
}

void* ModuleGetFunctionList(void* module)
{
	Module* modulePtr=(Module*)module;
	return modulePtr->functionList;
}

void* ModuleGetFieldList(void* module)
{
	Module* modulePtr=(Module*)module;
	return modulePtr->fieldList;
}

void* ModuleGetNameList(void* module)
{
	Module* modulePtr=(Module*)module;
	return modulePtr->nameList;
}

void dump(void* moduleList,void* globalFieldList,void* globalFunctionList)
{
	ModuleList* moduleListPtr=(ModuleList*)moduleList;
	for(int i=0;i<moduleListPtr->size();i++)
	{
		Module* module=(*moduleListPtr)[i];
		dumpModule(module);
	}
	
	printf("@module global\n");
	FieldList* globalFieldListPtr=(FieldList*)globalFieldList;
	for(int i=0;i<globalFieldListPtr->size();i++)
	{
		Field* field=(*globalFieldListPtr)[i];
		dumpField(field,NULL);
	}
	FunctionList* globalFunctionListPtr=(FunctionList*)globalFunctionList;
	for(int i=0;i<globalFunctionListPtr->size();i++)
	{
		Function* function=(*globalFunctionListPtr)[i];
		dumpFunction(function,NULL);
	}
}

const std::string trimPointers(const char* typeName)
{
	std::string name(typeName);
	while(name[name.size()-1]=='*'&&name.size()>0)
	{
		name.resize(name.size()-1);
	}
	return name;
}

const std::string pathBetweenTypeNames(const char* fromTypeName,const char* toTypeName,ModuleList* moduleList)
{
	std::string trimFrom=trimPointers(fromTypeName);
	std::string trimTo=trimPointers(toTypeName);
	//rerturn for sepcial basic types
	if(strcmp(toTypeName,"void")==0)
	{
		return std::string("#nil");
	}
	else if(strcmp(toTypeName,"bool")==0)
	{
		return std::string("#boolean");
	}
	else if(strcmp(toTypeName,"char*")==0||strcmp(toTypeName,"string")==0)
	{
		return std::string("#string");
	}
	else if(strcmp(toTypeName,"int")==0||strcmp(toTypeName,"long")==0
		||strcmp(toTypeName,"float")==0||strcmp(toTypeName,"double")==0)
	{
		return std::string("#number");
	}
	else if(strcmp(toTypeName,"array")==0)
	{
		return std::string("#table");
	}

	//for basic pointer types
	if(trimTo=="void"||trimTo=="bool"||trimTo=="char"||trimTo=="int"||trimTo=="long"||trimTo=="float"||trimTo=="double"||trimTo=="array")
	{
		return std::string("#")+std::string(toTypeName);
	}

	//if it is myself
	if(trimFrom==trimTo)
	{
		return std::string("#")+trimFrom;
	}
	//it is not myself
	//NOTICE:here does not check whether the type named by trimTo is in moduleList or not,since we can combine the pkg with pre parsed one
	if(trimFrom!=trimTo)
	{
		return trimTo+std::string("#")+trimTo;
	}
	//otherwise it the basic type
	return std::string("#")+std::string(toTypeName);
}

void saveLUA(const char* path,const char* moduleName,const char* parentName,ModuleList* moduleList,FieldList* fieldList,FunctionList* functionList)
{
	FILE* file=fopen(path,"w");
	if(file==NULL)
	{
		return;
	}

	const char* sepLine="--------------------------------";
	fprintf(file,"%s\n",sepLine);
	fprintf(file,"-- @module %s\n",moduleName);
	fprintf(file,"\n");

	for(int i=0;i<fieldList->size();i++)
	{
		Field* field=(*fieldList)[i];
		std::string pathOfFieldType=pathBetweenTypeNames(moduleName,field->typeName,moduleList);
		fprintf(file,"%s\n",sepLine);
		fprintf(file,"-- @field [parent=#%s] %s %s\n",parentName,pathOfFieldType.c_str(),field->variableName);
		fprintf(file,"\n");
	}
	
	for(int i=0;i<functionList->size();i++)
	{
		Function* function=(*functionList)[i];
		if(strcmp(function->functionName,moduleName)==0)
		{
			//TODO: create methods for constructor or destructor
			continue;
		}

		fprintf(file,"%s\n",sepLine);
		fprintf(file,"-- @function [parent=#%s] %s\n",parentName,function->functionName);
		ParamList* paramList=function->paramList;
		if(strcmp(parentName,"global")!=0)
		{
			//just add a 'self' param for all class functions,since tolua++ do this for all class functions
			fprintf(file,"-- @param self\n");
		}
		for(int j=0;j<paramList->size();j++)
		{
			Param* param=(*paramList)[j];
			if(strcmp(param->typeName,"")==0&&strcmp(param->paramName,"self")==0)
			{
				//do nothing here,since the 'self' param has been added for all class functions befor loop
			}
			else if(strcmp(param->typeName,"void")==0)
			{
				//do not output for void params
			}
			else
			{
				std::string pathOfParamType=pathBetweenTypeNames(moduleName,param->typeName,moduleList);
				fprintf(file,"-- @param %s %s\n",pathOfParamType.c_str(),param->paramName);
			}
		}
		std::string pathOfReturnType=pathBetweenTypeNames(moduleName,function->returnTypeName,moduleList);
		fprintf(file,"-- @return %s\n",pathOfReturnType.c_str());
		fprintf(file,"\n");
	}

	fprintf(file,"return nil\n");
	fclose(file);
}

void saveAUTO(ModuleList* moduleList,const char* path)
{
	FILE* file=fopen(path,"w");
	if(file==NULL)
	{
		return;
	}

	char functionStrBuffer[1000];
	for(int i=0;i<moduleList->size();i++)
	{
		Module* module=(*moduleList)[i];
		fprintf(file,"function to_%s(obj) tolua_cast(obj,\"%s\"); return obj; end;\n",module->moduleName,module->moduleName);
	}

	fclose(file);
}

void save(void* moduleList,const char* globalModuleName,void* globalFieldList,void* globalFunctionList,const char* outputDirName,const char* autoFileName)
{
	ModuleList* moduleListPtr=(ModuleList*)moduleList;
	FieldList* globalFieldListPtr=(FieldList*)globalFieldList;
	FunctionList* globalFunctionListPtr=(FunctionList*)globalFunctionList;

	char commandBuffer[100];
	sprintf(commandBuffer,"mkdir %s",outputDirName);
	system(commandBuffer);

	char pathBuffer[100];
	for(int i=0;i<moduleListPtr->size();i++)
	{
		Module* module=(*moduleListPtr)[i];
		const char* moduleName=module->moduleName;
		sprintf(pathBuffer,"%s/%s.lua",outputDirName,moduleName);
		FieldList* mergedFieldList=module->getMergedFieldList(moduleListPtr);
		FunctionList* mergedFunctionList=module->getMergedFunctionList(moduleListPtr);
		saveLUA(pathBuffer,moduleName,moduleName,moduleListPtr,mergedFieldList,mergedFunctionList);
	}

	//attach all module names to global environment for LDT
	for(int i=0;i<moduleListPtr->size();i++)
	{
		Module* module=(*moduleListPtr)[i];
		FieldListAddField(globalFieldListPtr,newField(module->moduleName,module->moduleName));
	}

	//attach all to_XXX functions to global evironment for LDT
	Param* param=(Param*)newParam("all","obj",0);
	ParamList* paramList=(ParamList*)newParamList();
	ParamListAddParam(paramList,param);
	char functionNameBuffer[100];
	for(int i=0;i<moduleListPtr->size();i++)
	{
		Module* module=(*moduleListPtr)[i];
		sprintf(functionNameBuffer,"to_%s",module->moduleName);
		Function* function=(Function*)newFunction(module->moduleName,strdup(functionNameBuffer),paramList);
		FunctionListAddFunction(globalFunctionListPtr,function);
	}

	//save global module
	sprintf(pathBuffer,"%s/%s.lua",outputDirName,globalModuleName);
	saveLUA(pathBuffer,globalModuleName,"global",moduleListPtr,globalFieldListPtr,globalFunctionListPtr);

	//save auto files
	sprintf(pathBuffer,"%s/%s.lua",outputDirName,autoFileName);
	saveAUTO(moduleListPtr,pathBuffer);
}
build:
	flex api_parser.l
	bison -v -d api_parser.y
	gcc -c -w *.c
	g++ -c *.cpp
	g++ *.o -o api_parser

clean:
	-rm *.o
	-rm *.yy.c
	-rm *.yy.cc
	-rm *.tab.h
	-rm *.tab.c
	-rm *.output

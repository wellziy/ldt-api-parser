%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//declare api from c++
#include "cpp_api.h"
//declare api from flex
void yyerror(const char* s);

void* s_module_list=NULL;
void* s_global_function_list=NULL;
void* s_global_field_list=NULL;
void* s_pkg_name_list=NULL;

%}

%union
{
	char* c_string;
	void* module;
	void* function;
	void* param_list;
	void* param;
	void* field_list;
	void* field;
	void* name_list;
};

%token CLASS
%token STRUCT

%token CONST 
%token STATIC

 /*
%token BOOL
%token VOID

%token CHAR
%token SHORT
%token INT
%token LONG
%token LONGLONG

%token FLOAT
%token DOUBLE
%token LUA_FUNCTION
 */

%token DEFINE
%token TYPEDEF
%token ENUM
%token PUBLIC
%token PROTECTED
%token PRIVATE

%token PINCLUDE
%token PFILE

%token BOOL_VALUE
%token NULL_PTR
%token <c_string> CSTRING
%token NUMBER

%token <c_string> IDENTIFIER
%token ERR

%type <module> class_entity
%type <name_list> parent_classes
%type <c_string> class_name
%type <module> class_members
%type <c_string> variable_type
%type <function> function_entity
%type <c_string> function_name
%type <param_list> param_list
%type <c_string> param_name
%type <field> variable_entity
%type <c_string> variable_name
%type <field> define_entity
%type <c_string> macro_name
%type <field_list> enum_entity
%type <c_string> enum_type_name
%type <field_list> enum_variable_list

%%
program:	class_entity ';' program			{ ModuleListAddModule(s_module_list,$1); }
|	action_scope function_entity ';' program	{ FunctionListAddFunction(s_global_function_list,$2); }
|	action_scope variable_entity ';' program	{ FieldListAddField(s_global_field_list,$2); }
|	define_entity program						{ FieldListAddField(s_global_field_list,$1); }
|	PINCLUDE CSTRING program					{}
|	PFILE CSTRING program  						{ const char* name=trimDoubleQuote($2); NameListAddName(s_pkg_name_list,name); }
|	action_scope enum_entity	';' program		{ FieldListMerge(s_global_field_list,$2); }
|	TYPEDEF enum_entity IDENTIFIER ';' program	{ FieldListMerge(s_global_field_list,$2); }
|	TYPEDEF variable_type variable_type ';' program
|	';' program
|
;

action_scope:	STATIC
|
;

class_entity:	CLASS class_name ':' parent_classes '{' class_members '}' 	{ $$=$6; ModuleSetModuleName($$,$2); ModuleSetNameList($$,$4); }
|	CLASS class_name '{' class_members '}'									{ $$=$4; ModuleSetModuleName($$,$2); }
;

parent_classes:		parent_classes ',' parent_control class_name	{ $$=$1; NameListAddName($$,$4); }
| parent_control class_name											{ $$=newNameList(); NameListAddName($$,$2); }
;

parent_control:		access_control
|
;

class_name:	IDENTIFIER	{ $$=$1; }
;

access_control:	PUBLIC
|	PROTECTED
|	PRIVATE
;

class_members:	access_control ':' STATIC function_entity ';' class_members	
{ 	
	$$=$6; void* functionList=ModuleGetFunctionList($$); 
	void* function=$4;
	FunctionListAddFunction(functionList,function);
}
|	STATIC function_entity ';' class_members
{
	$$=$4; void* functionList=ModuleGetFunctionList($$);
	void* function=$2;
	FunctionListAddFunction(functionList,function);
}
|	access_control ':' function_entity ';' class_members
{
	$$=$5; void* functionList=ModuleGetFunctionList($$);
	void* param=newParam("","self",0); void* function=$3;
	void* paramList=FunctionGetParamList(function);
	ParamListAddParamAtFront(paramList,param); 
	FunctionListAddFunction(functionList,function);
}
|	function_entity ';' class_members
{
	$$=$3; void* functionList=ModuleGetFunctionList($$);
	void* param=newParam("","self",0); void* function=$1;
	void* paramList=FunctionGetParamList(function);
	ParamListAddParamAtFront(paramList,param); 
	FunctionListAddFunction(functionList,function);
}
|	access_control ':' variable_entity ';' class_members
{
	$$=$5; void* fieldList=ModuleGetFieldList($$);
	void* field=$3;
	FieldListAddField(fieldList,field);
}
|	variable_entity ';' class_members
{
	$$=$3; void* fieldList=ModuleGetFieldList($$);
	void* field=$1;
	FieldListAddField(fieldList,field);
}
|	access_control ':' TYPEDEF enum_entity IDENTIFIER ';' class_members
{
	$$=$7; void* fieldList=ModuleGetFieldList($$);
	FieldListMerge(fieldList,$4);
}
|	TYPEDEF enum_entity IDENTIFIER ';' class_members
{
	$$=$5; void* fieldList=ModuleGetFieldList($$);
	FieldListMerge(fieldList,$2);
}
|	';' class_members
{
	$$=$2;
}
|
{
	$$=newModule("");
}
;

variable_type:	variable_type '*'	{ $$=typeNameForPointerOfType($1); }
|	IDENTIFIER						{ $$=$1; }
|	name_space ':' ':' IDENTIFIER	{ $$=$4; }
;

name_space:	IDENTIFIER
;

function_entity:	variable_type function_name '(' param_list ')' 	{ $$=newFunction($1,$2,$4); }
|	function_name '(' param_list ')'									{ /*for constructor*/ $$=newFunction("",$1,$3); }
|	'~' function_name '(' param_list ')'								{ /*for desstructor*/ $$=newFunction("~",$2,$4); }
;

function_name:	IDENTIFIER		{ $$=$1; }
|	IDENTIFIER '@' IDENTIFIER	{ $$=$3; }
;

param_list:	variable_type param_name ',' param_list	
{ 	
	$$=$4; void* param=newParam($1,$2,0); 
	ParamListAddParamAtFront($$,param); 
}
|	variable_type param_name '=' param_value ',' param_list
{
	$$=$6; void* param=newParam($1,$2,1);
	ParamListAddParamAtFront($$,param);
}
|	variable_type param_name
{
	$$=newParamList(); void* param=newParam($1,$2,0);
	ParamListAddParamAtFront($$,param);
}
|	variable_type param_name '=' param_value
{
	$$=newParamList(); void* param=newParam($1,$2,1);
	ParamListAddParamAtFront($$,param);
}
|
{
	$$=newParamList();
}
;

param_value:	BOOL_VALUE
|	NULL_PTR
|	CSTRING
|	NUMBER
|	IDENTIFIER
|	IDENTIFIER '(' param_value_list ')'
;

param_value_list:	param_value ',' param_value_list
|	param_value
|
;

param_name:	IDENTIFIER	{ $$=$1; }
|	{ $$=""; }
;

variable_entity:	variable_type variable_name	{ $$=newField($1,$2); }
|	variable_type variable_name '[' NUMBER ']'	{ $$=newField("array",$2); }
;

variable_name:	IDENTIFIER	{ $$=$1; }
;

define_entity:	DEFINE macro_name macro_value	{ $$=newField("define",$2); }
;

macro_name:		IDENTIFIER	{ $$=$1; }
;

macro_value:	NULL_PTR
|	BOOL_VALUE
|	NUMBER
|	CSTRING
|
;

enum_entity:	ENUM  enum_type_name '{' enum_variable_list '}'		{ $$=$4; }
;

enum_type_name:	IDENTIFIER	{ $$=$1; }
|	{ $$=""; }
;

enum_variable_list:		variable_name '=' enum_value enum_variable_list 
{
	$$=$4; void* field=newField("enum",$1);
	FieldListAddField($$,field);
}
|	variable_name enum_variable_list
{
	$$=$2; void* field=newField("enum",$1);
	FieldListAddField($$,field);
}
|	',' enum_variable_list
{
	$$=$2;
}
|
{
	$$=newFieldList();
}
;
	/*BUG: it will cause 6 shift/reduce conflicts here. */
enum_value:	NUMBER
|	IDENTIFIER
|	'(' enum_value ')'
|	'~' enum_value
|	enum_value '|' enum_value
|	enum_value '&' enum_value
;

%%

int main(int argc,char* argv[])
{
	if(argc!=5)
	{
		printf("params error\n");
		showUseage();
		return;
	}

	s_module_list=newModuleList();
	s_global_field_list=newFieldList();
	s_global_function_list=newFunctionList();
	s_pkg_name_list=newNameList();

	const char* inputFileName=argv[1];
	const char* outputDirName=argv[2];
	const char* globalModuleName=argv[3];
	const char* autoFileName=argv[4];

	NameListAddName(s_pkg_name_list,inputFileName);
	while(1)
	{
		const char* name=NameListPopName(s_pkg_name_list);
		if(name==NULL)
		{
			break;
		}
		parsePKG(name);
	}
	
	save(s_module_list,globalModuleName,s_global_field_list,s_global_function_list,outputDirName,autoFileName);
	return 0;
}
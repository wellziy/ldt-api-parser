/* A Bison parser, made by GNU Bison 2.4.2.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2006, 2009-2010 Free Software
   Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 1 "api_parser.y"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//declare api from c++
#include "cpp_api.h"
//declare api from flex
void yyerror(const char* s);

void* s_module_list=NULL;
void* s_global_function_list=NULL;
void* s_global_field_list=NULL;
void* s_pkg_name_list=NULL;



/* Line 189 of yacc.c  */
#line 89 "api_parser.tab.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     CLASS = 258,
     STRUCT = 259,
     CONST = 260,
     STATIC = 261,
     DEFINE = 262,
     TYPEDEF = 263,
     ENUM = 264,
     PUBLIC = 265,
     PROTECTED = 266,
     PRIVATE = 267,
     PINCLUDE = 268,
     PFILE = 269,
     BOOL_VALUE = 270,
     NULL_PTR = 271,
     CSTRING = 272,
     NUMBER = 273,
     IDENTIFIER = 274,
     ERR = 275
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 18 "api_parser.y"

	char* c_string;
	void* module;
	void* function;
	void* param_list;
	void* param;
	void* field_list;
	void* field;
	void* name_list;



/* Line 214 of yacc.c  */
#line 158 "api_parser.tab.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 170 "api_parser.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  24
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   191

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  36
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  26
/* YYNRULES -- Number of rules.  */
#define YYNRULES  82
/* YYNRULES -- Number of states.  */
#define YYNSTATES  173

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   275

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    35,     2,
      27,    28,    26,     2,    25,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    22,    21,
       2,    31,     2,     2,    30,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    32,     2,    33,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    23,    34,    24,    29,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     7,    12,    17,    20,    24,    28,    33,
      39,    45,    48,    49,    51,    52,    60,    66,    71,    74,
      76,    77,    79,    81,    83,    85,    92,    97,   103,   107,
     113,   117,   125,   131,   134,   135,   138,   140,   145,   147,
     153,   158,   164,   166,   170,   175,   182,   185,   190,   191,
     193,   195,   197,   199,   201,   206,   210,   212,   213,   215,
     216,   219,   225,   227,   231,   233,   235,   237,   239,   241,
     242,   248,   250,   251,   256,   259,   262,   263,   265,   267,
     271,   274,   278
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      37,     0,    -1,    39,    21,    37,    -1,    38,    47,    21,
      37,    -1,    38,    53,    21,    37,    -1,    55,    37,    -1,
      13,    17,    37,    -1,    14,    17,    37,    -1,    38,    58,
      21,    37,    -1,     8,    58,    19,    21,    37,    -1,     8,
      45,    45,    21,    37,    -1,    21,    37,    -1,    -1,     6,
      -1,    -1,     3,    42,    22,    40,    23,    44,    24,    -1,
       3,    42,    23,    44,    24,    -1,    40,    25,    41,    42,
      -1,    41,    42,    -1,    43,    -1,    -1,    19,    -1,    10,
      -1,    11,    -1,    12,    -1,    43,    22,     6,    47,    21,
      44,    -1,     6,    47,    21,    44,    -1,    43,    22,    47,
      21,    44,    -1,    47,    21,    44,    -1,    43,    22,    53,
      21,    44,    -1,    53,    21,    44,    -1,    43,    22,     8,
      58,    19,    21,    44,    -1,     8,    58,    19,    21,    44,
      -1,    21,    44,    -1,    -1,    45,    26,    -1,    19,    -1,
      46,    22,    22,    19,    -1,    19,    -1,    45,    48,    27,
      49,    28,    -1,    48,    27,    49,    28,    -1,    29,    48,
      27,    49,    28,    -1,    19,    -1,    19,    30,    19,    -1,
      45,    52,    25,    49,    -1,    45,    52,    31,    50,    25,
      49,    -1,    45,    52,    -1,    45,    52,    31,    50,    -1,
      -1,    15,    -1,    16,    -1,    17,    -1,    18,    -1,    19,
      -1,    19,    27,    51,    28,    -1,    50,    25,    51,    -1,
      50,    -1,    -1,    19,    -1,    -1,    45,    54,    -1,    45,
      54,    32,    18,    33,    -1,    19,    -1,     7,    56,    57,
      -1,    19,    -1,    16,    -1,    15,    -1,    18,    -1,    17,
      -1,    -1,     9,    59,    23,    60,    24,    -1,    19,    -1,
      -1,    54,    31,    61,    60,    -1,    54,    60,    -1,    25,
      60,    -1,    -1,    18,    -1,    19,    -1,    27,    61,    28,
      -1,    29,    61,    -1,    61,    34,    61,    -1,    61,    35,
      61,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    86,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    99,   100,   103,   104,   107,   108,   111,
     112,   115,   118,   119,   120,   123,   129,   135,   143,   151,
     157,   163,   168,   173,   178,   183,   184,   185,   188,   191,
     192,   193,   196,   197,   200,   205,   210,   215,   221,   226,
     227,   228,   229,   230,   231,   234,   235,   236,   239,   240,
     243,   244,   247,   250,   253,   256,   257,   258,   259,   260,
     263,   266,   267,   270,   275,   280,   285,   290,   291,   292,
     293,   294,   295
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "CLASS", "STRUCT", "CONST", "STATIC",
  "DEFINE", "TYPEDEF", "ENUM", "PUBLIC", "PROTECTED", "PRIVATE",
  "PINCLUDE", "PFILE", "BOOL_VALUE", "NULL_PTR", "CSTRING", "NUMBER",
  "IDENTIFIER", "ERR", "';'", "':'", "'{'", "'}'", "','", "'*'", "'('",
  "')'", "'~'", "'@'", "'='", "'['", "']'", "'|'", "'&'", "$accept",
  "program", "action_scope", "class_entity", "parent_classes",
  "parent_control", "class_name", "access_control", "class_members",
  "variable_type", "name_space", "function_entity", "function_name",
  "param_list", "param_value", "param_value_list", "param_name",
  "variable_entity", "variable_name", "define_entity", "macro_name",
  "macro_value", "enum_entity", "enum_type_name", "enum_variable_list",
  "enum_value", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,    59,    58,   123,   125,    44,    42,    40,    41,   126,
      64,    61,    91,    93,   124,    38
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    36,    37,    37,    37,    37,    37,    37,    37,    37,
      37,    37,    37,    38,    38,    39,    39,    40,    40,    41,
      41,    42,    43,    43,    43,    44,    44,    44,    44,    44,
      44,    44,    44,    44,    44,    45,    45,    45,    46,    47,
      47,    47,    48,    48,    49,    49,    49,    49,    49,    50,
      50,    50,    50,    50,    50,    51,    51,    51,    52,    52,
      53,    53,    54,    55,    56,    57,    57,    57,    57,    57,
      58,    59,    59,    60,    60,    60,    60,    61,    61,    61,
      61,    61,    61
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     3,     4,     4,     2,     3,     3,     4,     5,
       5,     2,     0,     1,     0,     7,     5,     4,     2,     1,
       0,     1,     1,     1,     1,     6,     4,     5,     3,     5,
       3,     7,     5,     2,     0,     2,     1,     4,     1,     5,
       4,     5,     1,     3,     4,     6,     2,     4,     0,     1,
       1,     1,     1,     1,     4,     3,     1,     0,     1,     0,
       2,     5,     1,     3,     1,     1,     1,     1,     1,     0,
       5,     1,     0,     4,     2,     2,     0,     1,     1,     3,
       2,     3,     3
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
      14,     0,    13,     0,     0,     0,     0,    14,     0,     0,
       0,    14,    21,     0,    64,    69,    72,    36,     0,     0,
       0,    14,    14,    11,     1,    36,     0,     0,     0,     0,
       0,     0,    14,     5,    20,    34,    66,    65,    68,    67,
      63,    71,     0,    35,     0,     0,     0,     6,     7,     0,
      42,     0,    62,     0,    60,    14,    48,    14,    14,     2,
      22,    23,    24,     0,     0,    19,     0,     0,    34,     0,
       0,     0,     0,    76,    14,     0,    14,    43,    48,    48,
       0,     3,    59,     0,     4,     8,    34,    20,    18,     0,
       0,     0,    33,     0,    16,    34,    34,    62,    76,    76,
       0,    10,    37,     9,     0,     0,     0,    58,    46,    40,
       0,     0,    34,     0,     0,     0,     0,     0,    28,    30,
      75,     0,    74,    70,    41,    39,    61,    48,     0,    15,
      17,    26,    34,     0,     0,    34,    34,    77,    78,     0,
       0,    76,    44,    49,    50,    51,    52,    53,    47,    32,
      34,     0,    27,    29,     0,    80,     0,     0,    73,    57,
      48,    25,    34,    79,    81,    82,    56,     0,    45,    31,
      57,    54,    55
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     8,     9,    10,    63,    64,    13,    69,    70,    27,
      19,    71,    29,    83,   166,   167,   108,    72,    99,    11,
      15,    40,    20,    42,   100,   141
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -84
static const yytype_int16 yypact[] =
{
      91,    -1,   -84,     3,    17,    55,    73,    91,   103,    26,
      90,    91,   -84,   115,   -84,   114,   101,   119,    59,   120,
     117,    91,    91,   -84,   -84,    49,   124,    76,   125,   118,
     126,   127,    91,   -84,   123,    13,   -84,   -84,   -84,   -84,
     -84,   -84,   121,   -84,    60,   128,   130,   -84,   -84,   133,
     129,   122,    92,   131,   132,    91,   134,    91,    91,   -84,
     -84,   -84,   -84,   -14,    -1,   -84,    22,   145,    13,   135,
     136,   140,   141,    28,    91,   144,    91,   -84,   134,   134,
     137,   -84,    87,   138,   -84,   -84,    13,   123,   -84,    95,
     146,   149,   -84,    25,   -84,    13,    13,   -84,    28,    18,
     147,   -84,   -84,   -84,   142,   148,   139,   -84,    42,   -84,
     150,    -1,    13,   152,    22,   145,   154,   156,   -84,   -84,
     -84,    21,   -84,   -84,   -84,   -84,   -84,   134,   109,   -84,
     -84,   -84,    13,   157,   160,    13,    13,   -84,   -84,    21,
      21,    58,   -84,   -84,   -84,   -84,   -84,   153,   158,   -84,
      13,   161,   -84,   -84,    81,   105,    21,    21,   -84,   109,
     134,   -84,    13,   -84,   105,   105,   159,   162,   -84,   -84,
     109,   -84,   -84
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -84,     6,   -84,   -84,   -84,    78,   -54,   -22,   -66,    -4,
     -84,    -6,   -21,   -71,    41,    11,   -84,    -5,   164,   -84,
     -84,   -84,    -8,   -84,   -83,   -39
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -43
static const yytype_int16 yytable[] =
{
      18,    31,    92,    28,    30,    51,    53,   104,   105,    86,
      88,    87,    65,    23,    44,   120,   122,    33,    12,    66,
     110,    67,    14,    60,    61,    62,    16,    47,    48,   118,
     119,   114,    25,   115,    68,    16,    17,    97,    59,   137,
     138,    25,    26,    98,    25,    25,   131,    97,   139,   121,
     140,    26,    82,    98,    26,    26,   142,   130,   158,    91,
      90,    81,    89,    84,    85,    65,   149,   127,    53,   152,
     153,   -38,    21,   128,    82,    82,   -42,    97,    17,    49,
     101,    74,   103,    98,   161,    43,    43,   116,   117,   168,
      22,   -12,   156,   157,     1,    52,   169,     2,     3,     4,
     154,   155,    43,    24,     5,     6,   107,   134,   133,   163,
      89,    32,     7,    43,    50,   156,   157,   164,   165,   -42,
      41,    43,    49,    82,   143,   144,   145,   146,   147,    36,
      37,    38,    39,    60,    61,    62,    46,    34,    35,   156,
     157,   -38,    45,    50,    73,    56,    55,    57,    58,    78,
      75,    76,    77,    17,    16,   106,    82,    93,    79,    49,
      94,    95,    96,   102,    80,   111,   109,   112,   113,   148,
     124,   123,   126,   132,   129,   135,   125,   136,   150,   151,
     159,   172,   162,   160,   170,     0,     0,     0,     0,     0,
     171,    54
};

static const yytype_int16 yycheck[] =
{
       4,     9,    68,     9,     9,    26,    27,    78,    79,    23,
      64,    25,    34,     7,    18,    98,    99,    11,    19,     6,
      86,     8,    19,    10,    11,    12,     9,    21,    22,    95,
      96,     6,    19,     8,    21,     9,    19,    19,    32,    18,
      19,    19,    29,    25,    19,    19,   112,    19,    27,    31,
      29,    29,    56,    25,    29,    29,   127,   111,   141,    67,
      66,    55,    66,    57,    58,    87,   132,    25,    89,   135,
     136,    22,    17,    31,    78,    79,    27,    19,    19,    30,
      74,    21,    76,    25,   150,    26,    26,    93,    93,   160,
      17,     0,    34,    35,     3,    19,   162,     6,     7,     8,
     139,   140,    26,     0,    13,    14,    19,   115,   114,    28,
     114,    21,    21,    26,    19,    34,    35,   156,   157,    27,
      19,    26,    30,   127,    15,    16,    17,    18,    19,    15,
      16,    17,    18,    10,    11,    12,    19,    22,    23,    34,
      35,    22,    22,    19,    23,    27,    21,    21,    21,    27,
      22,    21,    19,    19,     9,    18,   160,    22,    27,    30,
      24,    21,    21,    19,    32,    87,    28,    21,    19,   128,
      28,    24,    33,    21,    24,    21,    28,    21,    21,    19,
      27,   170,    21,    25,    25,    -1,    -1,    -1,    -1,    -1,
      28,    27
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     6,     7,     8,    13,    14,    21,    37,    38,
      39,    55,    19,    42,    19,    56,     9,    19,    45,    46,
      58,    17,    17,    37,     0,    19,    29,    45,    47,    48,
      53,    58,    21,    37,    22,    23,    15,    16,    17,    18,
      57,    19,    59,    26,    45,    22,    19,    37,    37,    30,
      19,    48,    19,    48,    54,    21,    27,    21,    21,    37,
      10,    11,    12,    40,    41,    43,     6,     8,    21,    43,
      44,    47,    53,    23,    21,    22,    21,    19,    27,    27,
      32,    37,    45,    49,    37,    37,    23,    25,    42,    45,
      47,    58,    44,    22,    24,    21,    21,    19,    25,    54,
      60,    37,    19,    37,    49,    49,    18,    19,    52,    28,
      44,    41,    21,    19,     6,     8,    47,    53,    44,    44,
      60,    31,    60,    24,    28,    28,    33,    25,    31,    24,
      42,    44,    21,    47,    58,    21,    21,    18,    19,    27,
      29,    61,    49,    15,    16,    17,    18,    19,    50,    44,
      21,    19,    44,    44,    61,    61,    34,    35,    60,    27,
      25,    44,    21,    28,    61,    61,    50,    51,    49,    44,
      25,    28,    51
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1464 of yacc.c  */
#line 86 "api_parser.y"
    { ModuleListAddModule(s_module_list,(yyvsp[(1) - (3)].module)); ;}
    break;

  case 3:

/* Line 1464 of yacc.c  */
#line 87 "api_parser.y"
    { FunctionListAddFunction(s_global_function_list,(yyvsp[(2) - (4)].function)); ;}
    break;

  case 4:

/* Line 1464 of yacc.c  */
#line 88 "api_parser.y"
    { FieldListAddField(s_global_field_list,(yyvsp[(2) - (4)].field)); ;}
    break;

  case 5:

/* Line 1464 of yacc.c  */
#line 89 "api_parser.y"
    { FieldListAddField(s_global_field_list,(yyvsp[(1) - (2)].field)); ;}
    break;

  case 6:

/* Line 1464 of yacc.c  */
#line 90 "api_parser.y"
    {;}
    break;

  case 7:

/* Line 1464 of yacc.c  */
#line 91 "api_parser.y"
    { const char* name=trimDoubleQuote((yyvsp[(2) - (3)].c_string)); NameListAddName(s_pkg_name_list,name); ;}
    break;

  case 8:

/* Line 1464 of yacc.c  */
#line 92 "api_parser.y"
    { FieldListMerge(s_global_field_list,(yyvsp[(2) - (4)].field_list)); ;}
    break;

  case 9:

/* Line 1464 of yacc.c  */
#line 93 "api_parser.y"
    { FieldListMerge(s_global_field_list,(yyvsp[(2) - (5)].field_list)); ;}
    break;

  case 15:

/* Line 1464 of yacc.c  */
#line 103 "api_parser.y"
    { (yyval.module)=(yyvsp[(6) - (7)].module); ModuleSetModuleName((yyval.module),(yyvsp[(2) - (7)].c_string)); ModuleSetNameList((yyval.module),(yyvsp[(4) - (7)].name_list)); ;}
    break;

  case 16:

/* Line 1464 of yacc.c  */
#line 104 "api_parser.y"
    { (yyval.module)=(yyvsp[(4) - (5)].module); ModuleSetModuleName((yyval.module),(yyvsp[(2) - (5)].c_string)); ;}
    break;

  case 17:

/* Line 1464 of yacc.c  */
#line 107 "api_parser.y"
    { (yyval.name_list)=(yyvsp[(1) - (4)].name_list); NameListAddName((yyval.name_list),(yyvsp[(4) - (4)].c_string)); ;}
    break;

  case 18:

/* Line 1464 of yacc.c  */
#line 108 "api_parser.y"
    { (yyval.name_list)=newNameList(); NameListAddName((yyval.name_list),(yyvsp[(2) - (2)].c_string)); ;}
    break;

  case 21:

/* Line 1464 of yacc.c  */
#line 115 "api_parser.y"
    { (yyval.c_string)=(yyvsp[(1) - (1)].c_string); ;}
    break;

  case 25:

/* Line 1464 of yacc.c  */
#line 124 "api_parser.y"
    { 	
	(yyval.module)=(yyvsp[(6) - (6)].module); void* functionList=ModuleGetFunctionList((yyval.module)); 
	void* function=(yyvsp[(4) - (6)].function);
	FunctionListAddFunction(functionList,function);
;}
    break;

  case 26:

/* Line 1464 of yacc.c  */
#line 130 "api_parser.y"
    {
	(yyval.module)=(yyvsp[(4) - (4)].module); void* functionList=ModuleGetFunctionList((yyval.module));
	void* function=(yyvsp[(2) - (4)].function);
	FunctionListAddFunction(functionList,function);
;}
    break;

  case 27:

/* Line 1464 of yacc.c  */
#line 136 "api_parser.y"
    {
	(yyval.module)=(yyvsp[(5) - (5)].module); void* functionList=ModuleGetFunctionList((yyval.module));
	void* param=newParam("","self",0); void* function=(yyvsp[(3) - (5)].function);
	void* paramList=FunctionGetParamList(function);
	ParamListAddParamAtFront(paramList,param); 
	FunctionListAddFunction(functionList,function);
;}
    break;

  case 28:

/* Line 1464 of yacc.c  */
#line 144 "api_parser.y"
    {
	(yyval.module)=(yyvsp[(3) - (3)].module); void* functionList=ModuleGetFunctionList((yyval.module));
	void* param=newParam("","self",0); void* function=(yyvsp[(1) - (3)].function);
	void* paramList=FunctionGetParamList(function);
	ParamListAddParamAtFront(paramList,param); 
	FunctionListAddFunction(functionList,function);
;}
    break;

  case 29:

/* Line 1464 of yacc.c  */
#line 152 "api_parser.y"
    {
	(yyval.module)=(yyvsp[(5) - (5)].module); void* fieldList=ModuleGetFieldList((yyval.module));
	void* field=(yyvsp[(3) - (5)].field);
	FieldListAddField(fieldList,field);
;}
    break;

  case 30:

/* Line 1464 of yacc.c  */
#line 158 "api_parser.y"
    {
	(yyval.module)=(yyvsp[(3) - (3)].module); void* fieldList=ModuleGetFieldList((yyval.module));
	void* field=(yyvsp[(1) - (3)].field);
	FieldListAddField(fieldList,field);
;}
    break;

  case 31:

/* Line 1464 of yacc.c  */
#line 164 "api_parser.y"
    {
	(yyval.module)=(yyvsp[(7) - (7)].module); void* fieldList=ModuleGetFieldList((yyval.module));
	FieldListMerge(fieldList,(yyvsp[(4) - (7)].field_list));
;}
    break;

  case 32:

/* Line 1464 of yacc.c  */
#line 169 "api_parser.y"
    {
	(yyval.module)=(yyvsp[(5) - (5)].module); void* fieldList=ModuleGetFieldList((yyval.module));
	FieldListMerge(fieldList,(yyvsp[(2) - (5)].field_list));
;}
    break;

  case 33:

/* Line 1464 of yacc.c  */
#line 174 "api_parser.y"
    {
	(yyval.module)=(yyvsp[(2) - (2)].module);
;}
    break;

  case 34:

/* Line 1464 of yacc.c  */
#line 178 "api_parser.y"
    {
	(yyval.module)=newModule("");
;}
    break;

  case 35:

/* Line 1464 of yacc.c  */
#line 183 "api_parser.y"
    { (yyval.c_string)=typeNameForPointerOfType((yyvsp[(1) - (2)].c_string)); ;}
    break;

  case 36:

/* Line 1464 of yacc.c  */
#line 184 "api_parser.y"
    { (yyval.c_string)=(yyvsp[(1) - (1)].c_string); ;}
    break;

  case 37:

/* Line 1464 of yacc.c  */
#line 185 "api_parser.y"
    { (yyval.c_string)=(yyvsp[(4) - (4)].c_string); ;}
    break;

  case 39:

/* Line 1464 of yacc.c  */
#line 191 "api_parser.y"
    { (yyval.function)=newFunction((yyvsp[(1) - (5)].c_string),(yyvsp[(2) - (5)].c_string),(yyvsp[(4) - (5)].param_list)); ;}
    break;

  case 40:

/* Line 1464 of yacc.c  */
#line 192 "api_parser.y"
    { /*for constructor*/ (yyval.function)=newFunction("",(yyvsp[(1) - (4)].c_string),(yyvsp[(3) - (4)].param_list)); ;}
    break;

  case 41:

/* Line 1464 of yacc.c  */
#line 193 "api_parser.y"
    { /*for desstructor*/ (yyval.function)=newFunction("~",(yyvsp[(2) - (5)].c_string),(yyvsp[(4) - (5)].param_list)); ;}
    break;

  case 42:

/* Line 1464 of yacc.c  */
#line 196 "api_parser.y"
    { (yyval.c_string)=(yyvsp[(1) - (1)].c_string); ;}
    break;

  case 43:

/* Line 1464 of yacc.c  */
#line 197 "api_parser.y"
    { (yyval.c_string)=(yyvsp[(3) - (3)].c_string); ;}
    break;

  case 44:

/* Line 1464 of yacc.c  */
#line 201 "api_parser.y"
    { 	
	(yyval.param_list)=(yyvsp[(4) - (4)].param_list); void* param=newParam((yyvsp[(1) - (4)].c_string),(yyvsp[(2) - (4)].c_string),0); 
	ParamListAddParamAtFront((yyval.param_list),param); 
;}
    break;

  case 45:

/* Line 1464 of yacc.c  */
#line 206 "api_parser.y"
    {
	(yyval.param_list)=(yyvsp[(6) - (6)].param_list); void* param=newParam((yyvsp[(1) - (6)].c_string),(yyvsp[(2) - (6)].c_string),1);
	ParamListAddParamAtFront((yyval.param_list),param);
;}
    break;

  case 46:

/* Line 1464 of yacc.c  */
#line 211 "api_parser.y"
    {
	(yyval.param_list)=newParamList(); void* param=newParam((yyvsp[(1) - (2)].c_string),(yyvsp[(2) - (2)].c_string),0);
	ParamListAddParamAtFront((yyval.param_list),param);
;}
    break;

  case 47:

/* Line 1464 of yacc.c  */
#line 216 "api_parser.y"
    {
	(yyval.param_list)=newParamList(); void* param=newParam((yyvsp[(1) - (4)].c_string),(yyvsp[(2) - (4)].c_string),1);
	ParamListAddParamAtFront((yyval.param_list),param);
;}
    break;

  case 48:

/* Line 1464 of yacc.c  */
#line 221 "api_parser.y"
    {
	(yyval.param_list)=newParamList();
;}
    break;

  case 58:

/* Line 1464 of yacc.c  */
#line 239 "api_parser.y"
    { (yyval.c_string)=(yyvsp[(1) - (1)].c_string); ;}
    break;

  case 59:

/* Line 1464 of yacc.c  */
#line 240 "api_parser.y"
    { (yyval.c_string)=""; ;}
    break;

  case 60:

/* Line 1464 of yacc.c  */
#line 243 "api_parser.y"
    { (yyval.field)=newField((yyvsp[(1) - (2)].c_string),(yyvsp[(2) - (2)].c_string)); ;}
    break;

  case 61:

/* Line 1464 of yacc.c  */
#line 244 "api_parser.y"
    { (yyval.field)=newField("array",(yyvsp[(2) - (5)].c_string)); ;}
    break;

  case 62:

/* Line 1464 of yacc.c  */
#line 247 "api_parser.y"
    { (yyval.c_string)=(yyvsp[(1) - (1)].c_string); ;}
    break;

  case 63:

/* Line 1464 of yacc.c  */
#line 250 "api_parser.y"
    { (yyval.field)=newField("define",(yyvsp[(2) - (3)].c_string)); ;}
    break;

  case 64:

/* Line 1464 of yacc.c  */
#line 253 "api_parser.y"
    { (yyval.c_string)=(yyvsp[(1) - (1)].c_string); ;}
    break;

  case 70:

/* Line 1464 of yacc.c  */
#line 263 "api_parser.y"
    { (yyval.field_list)=(yyvsp[(4) - (5)].field_list); ;}
    break;

  case 71:

/* Line 1464 of yacc.c  */
#line 266 "api_parser.y"
    { (yyval.c_string)=(yyvsp[(1) - (1)].c_string); ;}
    break;

  case 72:

/* Line 1464 of yacc.c  */
#line 267 "api_parser.y"
    { (yyval.c_string)=""; ;}
    break;

  case 73:

/* Line 1464 of yacc.c  */
#line 271 "api_parser.y"
    {
	(yyval.field_list)=(yyvsp[(4) - (4)].field_list); void* field=newField("enum",(yyvsp[(1) - (4)].c_string));
	FieldListAddField((yyval.field_list),field);
;}
    break;

  case 74:

/* Line 1464 of yacc.c  */
#line 276 "api_parser.y"
    {
	(yyval.field_list)=(yyvsp[(2) - (2)].field_list); void* field=newField("enum",(yyvsp[(1) - (2)].c_string));
	FieldListAddField((yyval.field_list),field);
;}
    break;

  case 75:

/* Line 1464 of yacc.c  */
#line 281 "api_parser.y"
    {
	(yyval.field_list)=(yyvsp[(2) - (2)].field_list);
;}
    break;

  case 76:

/* Line 1464 of yacc.c  */
#line 285 "api_parser.y"
    {
	(yyval.field_list)=newFieldList();
;}
    break;



/* Line 1464 of yacc.c  */
#line 1936 "api_parser.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1684 of yacc.c  */
#line 298 "api_parser.y"


int main(int argc,char* argv[])
{
	if(argc!=5)
	{
		printf("params error\n");
		showUseage();
		return;
	}

	s_module_list=newModuleList();
	s_global_field_list=newFieldList();
	s_global_function_list=newFunctionList();
	s_pkg_name_list=newNameList();

	const char* inputFileName=argv[1];
	const char* outputDirName=argv[2];
	const char* globalModuleName=argv[3];
	const char* autoFileName=argv[4];

	NameListAddName(s_pkg_name_list,inputFileName);
	while(1)
	{
		const char* name=NameListPopName(s_pkg_name_list);
		if(name==NULL)
		{
			break;
		}
		parsePKG(name);
	}
	
	save(s_module_list,globalModuleName,s_global_field_list,s_global_function_list,outputDirName,autoFileName);
	return 0;
}

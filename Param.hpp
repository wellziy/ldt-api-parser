#pragma once

#include <string>

class Param
{
public:
	const char* typeName;
	const char* paramName;
	int hasDefaultValue;
};
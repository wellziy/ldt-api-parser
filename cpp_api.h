#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void showUseage();
char* typeNameForPointerOfType(const char* type);
char* trimDoubleQuote(const char* name);
void parsePKG(const char* path);

/* store variables in field */
void* newField(const char* typeName,const char* variableName);
void* newFieldList();
void FieldListAddField(void* fieldList,void* field);
void FieldListMerge(void* fieldList,void* fieldList2);

/* store paramerters  in param*/
void* newParam(const char* typeName,const char* paramName,int hasDefaultValue);
void* newParamList();
void ParamListAddParam(void* paramList,void* param);
void ParamListAddParamAtFront(void* paramList,void* param);

/* store functions in function */
void* newFunction(const char* returnTypeName,const char* functionName,void* paramList);
void* FunctionGetParamList(void* function);
void* newFunctionList();
void FunctionListAddFunction(void* functionList,void* function);

/* store parent classe' names */
void* newNameList();
void NameListAddName(void* nameList,const char* name);
const char* NameListPopName(void* nameList);
int NameListGetSize(void* nameList);

/* store classes in module */
void* newModule(const char* moduleName);
void ModuleSetModuleName(void* module,const char* moduleName);
void ModuleSetNameList(void* module,void* nameList);

void* newModuleList();
void ModuleListAddModule(void* moduleList,void* module);

void* ModuleListGetModuleByName(void* moduleList,const char* moduleName);
void* ModuleGetFunctionList(void* module);
void* ModuleGetFieldList(void* module);
void* ModuleGetNameList(void* module);

void dump(void* moduleList,void* globalFieldList,void* globalFunctionList);
void save(void* moduleList,const char* globalModuleName,void* globalFieldList,void* globalFunctionList,const char* outputDirName,const char* autoFileName);
#ifdef __cplusplus
}
#endif